export interface Claim {
  claim: number,
  content: string,
  children: Claim[]
}
