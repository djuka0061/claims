import { readFileSync, writeFileSync } from 'fs';
import path from 'path';
import { Claim } from './common/models/claim';
import { ChildrenInformation } from './common/models/childrenInformation'

const rExp: RegExp = /[.]$/;
const numberRegExp: RegExp = /[-]{0,1}[\d]*[.]{0,1}[\d]+/g;

let claims: Claim[] = [];

export class Formater {
  public formatTextToJson(): Claim[] {

    const text = readFileSync(path.resolve('inputData/claims.txt')).toString().split('\n');
    let tempAppendText: string = '';

    text.forEach(element => {

      tempAppendText += element;

      if (element.match(rExp)) {

        const { childClaim, childParent } = this.findNumberInString(tempAppendText)

        if (childClaim) {
          const claim: Claim = {
            claim: childClaim,
            content: tempAppendText,
            children: []
          }

          if (childParent) {
            this.insertChildren(claims, claim, childParent)
          } else {
            claims.push(claim)
          }

        }

        tempAppendText = '';
      }
    });

    writeFileSync(path.resolve('results/formatedClaims.json'), JSON.stringify(claims))
    return claims;
  }


  public findNumberInString(text: string): ChildrenInformation {
    const findNumbers: string[] = text.match(numberRegExp)

    return {
      childClaim: findNumbers[0] ? parseInt(findNumbers[0]) : null,
      childParent: findNumbers[1] ? parseInt(findNumbers[1]) : null
    };
  }

  public async insertChildren(claims: Claim[], newClaim: Claim, parent: number): Promise<Claim[]> {
    await claims.forEach(element => {
      if (element.claim === parent) {
        element.children.push(newClaim);
      } else {
        this.insertChildren(element.children, newClaim, parent);
      }
    });

    return claims;
  }

}
